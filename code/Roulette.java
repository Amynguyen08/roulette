import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        RouletteWheel wheel = new RouletteWheel();
        Scanner sc = new Scanner(System.in);
        int playerWallet = 1000;
        // spinningn wheel
        System.out.println("Hello player. Would you like to start betting? Enter Y or N");
        String userFeelingLucky = sc.nextLine();
        if (userFeelingLucky.equals("Y")) {
            wheel.spin();
            int ballNum = wheel.getValue();
            boolean gameOn = false;
            int winnings = 0;
            int losses = 0;
            while (!gameOn) {
                // what number? how much money (has to be in range)
                System.out.println("What number would you like to bet on?: ");
                int userBet = sc.nextInt();
                System.out.println("How much money would you like to place on this bet?");
                int userBetMoney = sc.nextInt();
                boolean betIsGood = false;
                while (!betIsGood) {
                    if (userBetMoney > playerWallet || userBetMoney <= 0) {
                        System.out.println("Too high or too low of a bet. Try again:");
                        userBetMoney = sc.nextInt();
                    } else {
                        // if bet number is good, betIsGood is true
                        betIsGood = true;
                    }
                }
                // check if player won round or not
                if (userBet == ballNum) {
                    System.out.println("Lucky player! You guessed right.");
                    userBetMoney *= 35;
                    winnings += userBetMoney;
                    playerWallet += userBetMoney;
                } else {
                    System.out.println("You loss this round. The number was: " + ballNum + ".");
                    losses += userBetMoney;
                    playerWallet -= userBetMoney;
                }
                if (playerWallet <= 0) {
                    System.out.println("You are out of money! Game over.");
                    System.out.println("You won " + winnings + " and loss " + losses);
                    gameOn = true;
                } else {
                    // buffer
                    sc.nextLine();
                    // wanna bet again or exit game?
                    System.out.println("Wallet: " + playerWallet);
                    System.out.println("Make another bet? Enter Y or N");
                    String feelingSoLucky = sc.nextLine();

                    if (!feelingSoLucky.equals("Y")) {
                        System.out.println("Good game! You won " + winnings + " and loss " + losses + ".");
                        gameOn = true;
                    }
                }
            }
        }
        sc.close();
    }
}
