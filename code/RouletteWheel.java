import java.util.Random;

public class RouletteWheel{
    Random rand = new Random();
    private int lastNum = 0;

    public void spin(){
        this.lastNum = rand.nextInt(37);
    }

    public int getValue(){
        return this.lastNum;
    }
}